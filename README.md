# ZFS Prometheus Exporter

The ZFS Prometheus Exporter is a Python-based tool designed to collect and export ZFS filesystem statistics to Prometheus. This allows for comprehensive monitoring of ZFS pools, ARC cache metrics, and disk statistics within the Prometheus ecosystem.

## Features

- **ZFS Pool Metrics:** Monitors space utilization, fragmentation, and health status of ZFS pools.
- **ARC Cache Metrics:** Tracks ARC performance, including hit ratio, cache hits/misses, and memory throttle counts.
- **Disk Metrics:** Gathers disk I/O operations per second, throughput, latency, and load.
- **DMU TX and TXG Metrics:** Provides insights into ZFS's data management unit transaction and transaction group statistics.

## Prerequisites

- Python 3.x
- Access to a ZFS filesystem
- Prometheus setup for metrics collection

## Installation

1. **Clone the Repository:**

   ```shell
   git clone https://gitlab.com/hmccallum/prom_zfs_export.git
   cd prom_zfs_export
   ```
1. ** Install Dependencies:**

    ```shell
    pip install -r requirements.txt
    ```



## Usage

To start the exporter, run the following command in the terminal. This command starts the Flask application which serves the metrics on the specified port.

```shell
python zfs_prom_exporter.py
```

By default, the exporter runs on 0.0.0.0 at port 9100. Configure Prometheus to scrape metrics from this endpoint.

## Configuration

No additional configuration is required to start using the ZFS Prometheus Exporter. However, ensure your Prometheus instance is configured to scrape metrics from the exporter's /metrics endpoint.

## Contributing

Contributions are welcome! If you'd like to improve the ZFS Prometheus Exporter, please fork the repository, make your changes, and submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Attribution

This project was originally developed by Hunter McCallum. For any use of this work, approprite attribution should be given.